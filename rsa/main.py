#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""

    Context: MI44 pratical
    Author: Tao 'DePierre' S.
    Issue:
        Given n and e, decrypt a RSA-encrypted text.
        Then, decrypt the result, previously encrypted by a homemade algo.

"""


def get_prime_factors(n):
    """Find the prime factors p and q composing n"""

    for x in range(int(n**0.5), 3, -2):
        if n % x == 0:
            p = x
            break
    q = n / p

    return [p, q]


def get_phi(p, q):
    """Return phi"""

    return int((p - 1) * (q - 1))


def egcd(a, b):
    """Extended Euclidian algorithm"""

    x, y, u, v = 0, 1, 1, 0
    while a != 0:
        q, r = b // a, b % a
        m, n = x - u * q, y - v * q
        b, a, x, y, u, v = a, r, u, v, m, n

    return max(x, y)


def decrypt_rsa(cyphertext, n, e):
    """Decrypt RSA algorithm"""

    print 'Given n = %i and e = %i' % (n, e)
    p, q = get_prime_factors(n)
    print 'We find the prime factors p = %i and q = %i' % (p, q)
    d = egcd(e, get_phi(p, q))
    print 'So the secret key d = %i' % (d)

    plaintext = []
    for block in cyphertext.split():
        plaintext.append(int(int(block)**d % n))

    return plaintext


def decrypt_mi44(cyphertext):
    """Decrypt MI44 homemade algorithm"""
    plain_to_cyph = {c: ord(c) % ord('A') for c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'}
    plain_to_cyph.update({' ': 26, '.': 27, ',': 28, "'": 29})
    cyph_to_plain = {plain_to_cyph[key]: key for key in plain_to_cyph}

    plaintext = ''
    for block in cyphertext:
        for i in range(2, -1, -1):
            plaintext += cyph_to_plain[block / 30**i]
            block -= plain_to_cyph[plaintext[-1]] * 30**i

    return plaintext


if __name__ == '__main__':
    cyphertext = '30667 53268 51818 59080 36734 37003 1184 280 1674 61934 36260 61869 2958 46846 41683 30667 66805 16105 18037 6485 37951 56027 68066 41041 68554 1918 10489 46330 30720 49602 68374 13460 4444'
    n = 70613
    e = 16633
    decrypted_rsa = decrypt_rsa(cyphertext, n, e)
    print
    print 'RSA decrypted:'
    print decrypted_rsa
    print
    print 'MI44 decrypted:'
    print decrypt_mi44(decrypted_rsa)
